<?php
require_once __DIR__ . '/../vendor/autoload.php';

use mef\Http\ServerRequest;
use mef\Http\FileStream;
use mef\Http\Response;
use mef\Http\Uri;
use mef\Config\FileLoader\PhpFileLoader;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

/**
 * Render the PSR-7 response.
 *
 * @param  Psr\Http\Message\ResponseInterface $response
 */
function renderResponse(ResponseInterface $response)
{
	http_response_code($response->getStatusCode());

	foreach ($response->getHeaders() as $key => $values)
	{
		foreach ($values as $i => $value)
		{
			header("$key: $value", $i === 0);
		}
	}

	echo $response->getBody()->getContents();
}

/**
 * Iterate over the $data, passing each value to $try. Return the first value
 * that is successfully returned by $try. If nothing succeeds, call and return
 * the value from $default.
 *
 * @param  mixed    $data    any traversable data
 * @param  callable $try     code to execute for each element
 * @param  callable $default default code to run if nothing is found
 *
 * @return mixed
 */
function getFirstOrDefault($data, callable $try, callable $default)
{
	foreach ($data as $value)
	{
		try
		{
			return $try($value);
		}
		catch (Exception $e)
		{
		}
	}

	return $default();
}

$config = (new PhpFileLoader)->loadFile(__DIR__ . '/../config/settings.php');

$request = ServerRequest::fromGlobals(
	new FileStream(fopen('php://input', 'r')),
	$_SERVER,
	$_GET,
	$_POST,
	$_COOKIE,
	$_FILES
);

if ($request->getMethod() !== 'GET')
{
	$response = Response::fromArray([
		'headers' => [
			'Content-Type' => 'text/plain'
		],
		'body' => 'Method not supported',
		'code' => 405
	]);
}
else
{
	$client = new Client;

	$relativeUrl = $request->getUri()->getPath();

	if ($request->getUri()->getQuery() !== '')
	{
		$relativeUrl .= '?' . $request->getUri()->getQuery();
	}

	$response = getFirstOrDefault($config['sources'],

		function ($sourceUrl) use ($client, $request, $relativeUrl)
		{
			return $client->send(
				$request->withoutHeader('host')->withUri(
					Uri::fromString(rtrim($sourceUrl, '/') . $relativeUrl)
				)
			);
		},

		function ()
		{
			return Response::fromArray([
				'headers' => [
					'Content-Type' => 'text/plain'
				],
				'body' => 'File not found',
				'code' => 404
			]);
		}
	);

	$response = $response->withoutHeader('Transfer-Encoding');
}

renderResponse($response);